// -- VARIABLES
// Ask the user's email
const user_mail = prompt("Insert your MAIL addres");
// Chars accepted
const accepted_characters = "_1234567890"
const accepted_characters_after_arroba = "qwertyuiopasdfghjklzxcvbnm"
// Dields separator
const arroba = "@"
const dot = "."
// Messages
const wrong_message = "WRONG mail!!!";
// -- CHECK FIELD SEPARATOR --
if (user_mail.indexOf(arroba) == -1 || user_mail.indexOf(dot) == -1) { // There aren't any arroba an any dot
	alert(wrong_message);
} else {
	// Get the positions of arroba and the dot
	const arroba_pos = user_mail.indexOf(arroba);
	const dot_pos = user_mail.indexOf(dot);
	// Check if there's only one arroba, one dot and arroba is at least two positions before the dot
	if (((arroba_pos + 1) >= dot_pos) || (user_mail.indexOf(arroba, arroba_pos + 1) != -1) || (user_mail.indexOf(dot, dot_pos + 1) != -1)) {
		alert(wrong_message);
	} else {
		// -- CHECKING FIELDS --
		// Separate the mail fields
		const before_arroba = user_mail.substring(0, arroba_pos);
		const between_arroba_dot = user_mail.substring(arroba_pos + 1, dot_pos);
		const after_dot = user_mail.substring(dot_pos + 1);
		// The AFTER DOT field must have a MAX LENGTH of 3
		if(after_dot.length > 3) {
			alert(wrong_message);
		} else {
			// Before arroba field check
			let i = 0;
			while ((i < before_arroba.length) && ((accepted_characters.indexOf(before_arroba.charAt(i) != -1))	
					|| ((accepted_characters_after_arroba.indexOf(before_arroba.charAt(i)) != -1) 
						|| accepted_characters_after_arroba.toUpperCase().indexOf(before_arroba.charAt(i)) != -1))) {
				i++;
			}
			// If all it's correct
			if (i < before_arroba.length) {
				alert(wrong_message);
			} else {
				// Between arroba and dot field check
				i = 0;
				while ((i < between_arroba_dot.length) && ((accepted_characters_after_arroba.indexOf(between_arroba_dot.charAt(i)) != -1) 
						|| (accepted_characters_after_arroba.toUpperCase().indexOf(between_arroba_dot.charAt(i)) != -1))) {
					i++;
				}
				if (i < between_arroba_dot.length) {
					alert(wrong_message);
				} else {
					alert("This mail is valid!!!!");
				}
			}
		}
	}
}


